<?php

include 'NumberApiSDK.php';

    $apiKey = isset($_POST['key'])? $_POST['key'] : '';
    $apiUrl = 'https://test.xn--812-5cdaa0ahc3eicnwi.xn--p1ai/api/v1';
    $api = new NumberApiSDK($apiKey, $apiUrl);

    // получение списка доступных номеров для вывода
    $phones = $api->getNumbers();

    // получение списка периодов оплаты
    $periods = $api->getPeriods();

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Пример использования API - форма заказа</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h3>Фома заказа клиентом</h3>
    <form action="order.php" method="post">
        <input type="hidden" name="key" value="<?=$apiKey;?>">
        <pre>
            <code class="language-php" data-lang="php">
// подключение к API
$api = new NumberApiSDK('<?=$apiKey;?>', '<?=$apiUrl;?>');

// получение списка доступных номеров для вывода
    $phones = $api->getNumbers();

// получение списка периодов оплаты
$periods = $api->getPeriods();

// вывод списка номеров
&lt;select name="phone"&gt;
    &lt;?php
    foreach ($phones as $p) {
        echo '&lt;option value="'.$p['number'].'"&gt;'.$p['number_formated'].'&lt;/option&gt;';
    }
    ?&gt;
&lt;/select&gt;
            </code>
        </pre>
        <div class="form-group">
            <label for="phoneNumber">Номер</label>
            <select required id="phoneNumber" name="phone" class="form-control">
                <?php
                foreach ($phones as $p) {
                    echo '<option value="'.$p['number'].'">'.$p['number_formated'].'</option>';
                }
                ?>
            </select>
        </div>
        <pre>
            <code class="language-php" data-lang="php">
// получение списка периодов оплаты
$periods = $api->getPeriods();
            </code>
        </pre>
        <div class="form-group">
            <label for="period">Период</label>
            <select required id="period" name="period_id" class="form-control">
                <?php
                foreach ($periods as $p) {
                    echo '<option value="'.$p['id'].'">'.$p['name'].' '.$p['price'].' руб.</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="clientName">Имя клиента</label>
            <input required type="text" id="clientName" name="client_name" class="form-control" value="">
        </div>
        <div class="form-group">
            <label for="clientEmail">Email клиента</label>
            <input required type="email" id="clientName" name="client_email" class="form-control" value="">
        </div>
        <div class="form-group">
            <label for="clientPhone">Номер клиента</label>
            <input required type="tel" id="clientPhone" name="client_phone" class="form-control" value="">
        </div>
        <div class="form-group">
            <button class="btn btn-default" type="submit">Заказать номер</button>
        </div>
    </form>
</div>
</body>
</html>