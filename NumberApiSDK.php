<?php

/**
 * Created by PhpStorm.
 * User: aliance
 * Date: 3/10/17
 * Time: 8:19 PM
 */
class NumberApiSDK
{

    const ORDER_STATUS_NEW = 0;
    const ORDER_STATUS_CODE = 5;
    const ORDER_STATUS_CONFIRMED = 10;
    const ORDER_STATUS_PAID = 20;
    const ORDER_STATUS_ENDED = 30;
    const ORDER_STATUS_CANCELED = -10;

    const REDIRECT_STATUS_FREE = 0;
    const REDIRECT_STATUS_PRELOCKED = 5;
    const REDIRECT_STATUS_LOCKED = 10;
    const REDIRECT_STATUS_LINKED = 20;
    const REDIRECT_STATUS_POSTLINKED = 30;

    protected $apiUrl = 'https://test.xn--812-5cdaa0ahc3eicnwi.xn--p1ai/api/v1/';

    protected $apiKey;

    protected $headers;
    protected $code;

    public function __construct($key, $url = '') {
        $this->apiKey = $key;

        if ($url) {
            $this->apiUrl = rtrim($url, '/').'/';
        }
    }

    protected function apiCall($path, $method = 'GET', $get = null, $body = null) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
        if ($get) {
            $path .= '?'.$get;
        }
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl.$path);

        $curlMethod = CURLOPT_HTTPGET;
        $curlValue = true;
        switch ($method) {
            case 'GET':
                $curlMethod = CURLOPT_HTTPGET;
                break;
            case 'POST':
                $curlMethod = CURLOPT_POST;
                break;
            case 'PUT':
                $curlMethod = CURLOPT_CUSTOMREQUEST;
                $curlValue = "PUT";
                break;
            case 'DELETE':
                $curlMethod = CURLOPT_CUSTOMREQUEST;
                $curlValue = "DELETE";
                break;
        }
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Basic '.base64_encode($this->apiKey.':'),
        );
        curl_setopt($ch, $curlMethod, $curlValue);
        curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($method == 'POST' || $method == 'PUT') {
            $body = json_encode($body);
            $headers[] = 'Content-Length: ' . strlen($body);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if (strpos($this->apiUrl, 'test') !== false) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        $response = curl_exec($ch);

        $this->code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $parts = preg_split('|(?:\r?\n){2}|m', $response, 2);
        $headers = $parts[0];
        $body = $parts[1];

        $headers = explode("\n", $headers);

        foreach ($headers as $h) {
            $h = trim($h);
            if ($h) {
                $h = explode(':', $h, 2);
                if (count($h) == 2) {
                    $this->headers[$h[0]] = trim($h[1]);
                    if ($h[0] == 'Content-Type' && strpos($h[1], 'application/json')) {
                        $body = json_decode($body, true);
                    }
                }
            }
        }

        return $body;
    }

    public function getOrders($search) {
        return $this->apiCall('order', 'GET', http_build_query($search));
    }

    public function saveOrder($data) {
        return $this->apiCall('order', 'POST', null, $data);
    }

    public function checkOrder($id) {
        return $this->apiCall('order/'.$id.'/check', 'GET');
    }

    public function cancelOrder($id) {
        return $this->apiCall('order/'.$id, 'DELETE');
    }

    public function getNumbers() {
        return $this->apiCall('number', 'GET');
    }

    public function getPeriods() {
        return $this->apiCall('period', 'GET');
    }

    public function getLastCode() {
        return $this->code;
    }

    public function getLastHeaders() {
        return $this->headers;
    }
}