<?php

include 'NumberApiSDK.php';

    $apiKey = isset($_POST['key'])? $_POST['key'] : '';
    $apiUrl = 'https://test.xn--812-5cdaa0ahc3eicnwi.xn--p1ai/api/v1';
    $api = new NumberApiSDK($apiKey, $apiUrl);

    /* заказ номера - берем все данные из предыдущей формы */
    $data = array(
        'period_id' => isset($_POST['period_id'])? intval($_POST['period_id']) : 0,
        'phone' => isset($_POST['phone'])? $_POST['phone'] : 0,
        'client_name' => isset($_POST['client_name'])? $_POST['client_name'] : 0,
        'client_email' => isset($_POST['client_email'])? $_POST['client_email'] : 0,
        'client_phone' => isset($_POST['client_phone'])? $_POST['client_phone'] : 0,
    );

//    // простая проверка, нужна более серьезная
    if (min($data)) {
        $result = $api->saveOrder($data);

        // мы получили положительный ответ - заказ создан
        if ($api->getLastCode() >= 200 && $api->getLastCode() < 300) {
            $orderId = $result['id'];
            // так можно узнать данные заказа
            $orderData = $api->checkOrder($orderId);
            // так можно аннулировать заказ
            $orderCancel = $api->cancelOrder($orderId);
        } else {
            // не получилось создать заказ - причина должна быть в переменной $result
            $orderData = false;
            $orderCancel = false;
        }
        // так получаем список сделанных заказов по фильтру: данном случае фильтруем по статусу
        $orderList = $api->getOrders(array('status' => NumberApiSDK::ORDER_STATUS_PAID));
    } else {
        // не достаточно данных
        $result = false;
        $orderData = false;
        $orderCancel = false;
        $orderList = false;
    }


?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Пример использования API - Работа с заказом</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h3>Заказ номера</h3>
    <pre>
        <code class="language-php" data-lang="php">
            $api = new NumberApiSDK('yourkey', 'api-url');
            /* заказ номера */
            $data = array(
                'period_id' => '<?=$data['period_id'];?>',
                'phone' => '<?=$data['phone'];?>',
                'client_name' => '<?=$data['client_name'];?>',
                'client_email' => '<?=$data['client_email'];?>',
                'client_phone' => '<?=$data['client_phone'];?>',
            );

            // простая проверка, нужна более серьезная
            if (min($data)) {
                $result = $api->saveOrder($data);
                if ($api->getLastCode() == 200) {
                    // заказ создан и мы имеем его данные:
                    $orderId = $result['id'];
                }
            }
        </code>
    </pre>
    <p>Результат вывода print_r($result):</p>
    <pre>
        <?php print_r($result);?>
    </pre>
    <?php if (isset($result['id'])) : ?>
        <dl class="dl-horizontal">
            <dt>id заказа</dt>
            <dd><?=$result['id'];?></dd>
            <dt>Статус</dt>
            <dd><?=$result['status'];?></dd>
        </dl>
    <?php else : ?>
        <p>Заказ не был создан (смотри $result['message'])</p>
        <code><?=$result['message'];?></code>
    <?php endif; ?>

    <h3>Проверка только что созданного заказа</h3>
    <?php if ($orderData) : ?>
        <pre>
            <code class="language-php" data-lang="php">
                $api = new NumberApiSDK('yourkey', 'api-url');
                $orderData = $api->checkOrder($orderId);
            </code>
        </pre>
        <h4>Результат</h4>
        <pre>
            <?php print_r($orderData); ?>
        </pre>
    <?php else : ?>
        <p>Заказ не был создан</p>
    <?php endif; ?>

    <h3>Вывести список заказов</h3>
    <pre>
        <code class="language-php" data-lang="php">
            $api = new NumberApiSDK('yourkey', 'api-url');
            // например текущие переадресации
            $orderList = $api->getOrders(array('status' => NumberApiSDK::ORDER_STATUS_PAID));
        </code>
    </pre>
    <h4>Результат</h4>
    <pre>
        <?php print_r($orderList); ?>
    </pre>

    <h3>Отменить заказ</h3>
    <?php if ($orderData) : ?>
        <pre>
            <code class="language-php" data-lang="php">
                $api = new NumberApiSDK('yourkey', 'api-url');
                $orderCancel = $api->cancelOrder($orderId);
            </code>
        </pre>
        <h4>Результат</h4>
        <pre>
            <?php print_r($orderCancel); ?>
        </pre>
    <?php else : ?>
        <p>Заказ не был создан</p>
    <?php endif; ?>

    <p></p>
    <p><a href="index.html">Начать сначала</a> </p>
</div>
</body>
</html>